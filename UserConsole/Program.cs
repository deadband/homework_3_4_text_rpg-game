﻿using Ninject;
using GameLogic;

namespace UserConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var kernel = new StandardKernel(new DiModule());
            var rpgGame = kernel.Get<RpgGame>();
            rpgGame.LetsPlayTheGame();
        }
    }
}