﻿using GameLogic.Interfaces;
using System;
using System.Threading;

namespace UserConsole
{
    class RpgGame
    {
        private IAddLogic _addLogic;
        private IGetLogic _getLogic;
        private IActionsService _actionsService;
        private Random _random;
        private int _playerSelection;
        private int _playerId;

        public RpgGame(IAddLogic addLogic, IGetLogic getLogic, IActionsService actionsService)
        {
            _addLogic = addLogic;
            _getLogic = getLogic;
            _actionsService = actionsService;
            _random = new Random();
        }

        private void DisplayMainMenu()
        {
            Console.Clear();
            Console.WriteLine("RPG GAME APP");
            Console.WriteLine();
            Console.WriteLine("1. Create player.");
            Console.WriteLine("2. Select player.");
            Console.WriteLine("3. Create monster.");
            Console.WriteLine("4. Simulate Game");
            Console.WriteLine("5. Display highscores.");
            Console.WriteLine("6. Display game history.");
            Console.WriteLine("7. Exit to Windows");
        }

        public void LetsPlayTheGame()
        {
            var menuSelection = 0;
            var maxMenuSelection = 7;
            while (menuSelection != maxMenuSelection)
            {
                DisplayMainMenu();
                menuSelection = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, maxMenuSelection);

                switch (menuSelection)
                {
                    case 1:
                        AddPlayer();
                        break;
                    case 2:
                        SelectPlayer();
                        break;
                    case 3:
                        AddMonster();
                        break;
                    case 4:
                        if (!ConditionsMetToStartGame()) break;
                        SimulateGame();
                        break;
                    case 5:
                        ShowHighScores();
                        break;
                    case 6:
                        ShowGameStory();
                        break;
                }
            }
        }

        private void AddPlayer()
        {
            Console.WriteLine("Enter character's name: ");
            var name = VerificationMethods.VerifyString(Console.ReadLine());
            Console.WriteLine(_addLogic.CreatePlayer(name));
            Thread.Sleep(1500);
            _playerSelection = _getLogic.GetPlayerCount();
        }

        private void SelectPlayer()
        {
            if (_getLogic.GetPlayerCount() == 0)
            {
                Console.WriteLine("Create player First!");
                Thread.Sleep(1500);
                return;
            }
            Console.Write(_getLogic.DisplayPlayers());
            Console.Write($"Select player [1-{_getLogic.GetPlayerCount()}]: ");
            _playerSelection = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, _getLogic.GetPlayerCount());
        }

        private void AddMonster()
        {
            Console.WriteLine("Enter monsters's name: ");
            var name = VerificationMethods.VerifyString(Console.ReadLine());
            Console.WriteLine("Enter monsters's level [1-15]: ");
            var level = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, 15);
            Console.WriteLine(_addLogic.CreateMonster(name, level));
            Thread.Sleep(1500);
        }

        private bool ConditionsMetToStartGame()
        {
            if (_getLogic.GetPlayerCount() == 0 || _getLogic.GetMonsterCount() == 0)
            {
                Console.WriteLine("Insufficient amount of players/monsters to start a game.");
                Thread.Sleep(1500);
                return false;
            }
            if (_playerSelection == 0)
            {
                Console.WriteLine("Select player First!");
                Thread.Sleep(1500);
                return false;
            }
            return true;
        }

        private void SimulateGame()
        {
            _playerId =_actionsService.SetActualPlayerAndStoryFile(_playerSelection);
            while (!_actionsService.IsPlayerDead(_playerId))
            {
                var randomAction = _random.NextDouble();
                switch (randomAction)
                {
                    case double n when (n < 0.3):
                        SimulateFightOrRetreat();                   break;
                    case double n when (n >= 0.3 && n < 0.4):
                        _actionsService.HpPotionFound(_playerId);            break;
                    case double n when (n >= 0.4 && n < 0.5):
                        _actionsService.MaxHpOrLevelUpItemFound(_playerId);  break;
                    case double n when (n >= 0.5 && n < 0.57):
                        _actionsService.MagicItemFound(_playerId);           break;
                    case double n when (n >= 0.57 && n < 0.64):
                        WeaponFound();                              break;
                    case double n when (n >= 0.64 && n < 0.71):
                        ArmorFound();                               break;
                    case double n when (n >= 0.71 && n < 0.78):
                        _actionsService.MagicItemLost(_playerId);            break;
                    case double n when (n >= 0.78 && n < 0.85):
                        _actionsService.WeaponLost(_playerId);               break;
                    case double n when (n >= 0.85 && n < 0.92):
                        _actionsService.ArmorLost(_playerId);                break;
                    default:
                        _actionsService.RandomNegativeEvent(_playerId);      break;
                }
            }
            _actionsService.GameOver(_playerId);
        }

        private void SimulateFightOrRetreat()
        {
            var monsterId = _actionsService.SetRandomMonster(_playerId);
            Console.Write("Select: 1. Fight, 2. Retreat: ");
            var choice = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, 2);
            if (choice == 1)
            {
                _actionsService.SimulateFight(monsterId, _playerId);

                if (!_actionsService.IsPlayerDead(_playerId)) // jak zyje po walce to niewielkie prawdopodobienstwo znalezienia czegoś
                {
                    var possibilityOfFindingMagicItemAfterFight = _random.NextDouble();
                    switch (possibilityOfFindingMagicItemAfterFight)
                    {
                        case double n when (n >= 0.15 && n < 0.2):
                            WeaponFound();
                            break;
                        case double n when (n >= 0.9 && n < 0.95):
                            ArmorFound();
                            break;
                        case double n when (n >= 0.63 && n < 0.68):
                            _actionsService.MagicItemFound(_playerId);
                            break;
                        default:
                            _actionsService.NothingFoundAfterFight(_playerId);
                            break;
                    }
                }
            }
            if (choice == 2) _actionsService.Retreat(monsterId, _playerId);
        }

        private void WeaponFound()
        {
            var weaponId = _actionsService.RandomWeaponFound(_playerId);
            Console.Write("Select: 1. Take and replace old one 2. Drop new and leave old one: ");
            var choice = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, 2);
            if (choice == 1) _actionsService.ReplaceWeapon(weaponId, _playerId);
            else _actionsService.ItemDroppedByPlayer(_playerId);
        }

        private void ArmorFound()
        {
            var armorId = _actionsService.RandomArmorFound(_playerId);
            Console.Write("Select: 1. Take and replace old one 2. Drop new and leave old one: ");
            var choice = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, 2);
            if (choice == 1) _actionsService.ReplaceArmor(armorId, _playerId);
            else _actionsService.ItemDroppedByPlayer(_playerId);
        }

        private void ShowHighScores()
        {
            if (!AnyResultsSaved()) return;
            Console.Write("\n" + _getLogic.DisplayHighscores());
            Console.ReadKey();
        }

        private void ShowGameStory()
        {
            if (!AnyResultsSaved()) return;
            Console.Write("\n" + _getLogic.DisplaySavedGameStories());
            Console.Write($"Select a story [1-{_getLogic.GetGameResultCount()}]: ");
            var storyNumber = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, _getLogic.GetGameResultCount());
            Console.WriteLine("\n" + _getLogic.DisplayGameStory(storyNumber));
            Console.ReadKey();
        }

        private bool AnyResultsSaved()
        {
            _getLogic.UpdateGameResultsInDataBase();
            if (_getLogic.GetGameResultCount() == 0)
            {
                Console.WriteLine("No game results saved.");
                Thread.Sleep(1500);
                return false;
            }
            return true;
        }
    }
}