﻿using System;

namespace UserConsole
{
    class VerificationMethods
    {
        public static int VerifyInteger(string number, int min, int max)
        {
            var parsePossible = int.TryParse(number, out int result);
            while (!parsePossible || result > max || result < min)
            {
                Console.Write("Incorrect number entered. Type again: ");
                number = Console.ReadLine();
                parsePossible = int.TryParse(number, out result);
            }
            return result;
        }

        public static string VerifyString(string text)
        {
            while (string.IsNullOrWhiteSpace(text) || string.IsNullOrEmpty(text))
            {
                Console.WriteLine("Wrong text entered. Please type again: ");
                text = Console.ReadLine();
            }
            return text;
        }
    }
}