﻿using NUnit.Framework;
using Moq;
using DataAccess.Repos.Interfaces;
using DataAccess.Entity;
using System.Collections.Generic;
using GameLogic.Interfaces;

namespace GameLogic.Tests
{
    [TestFixture]
    public class ActionsServiceTests
    {
        private Mock<IPlayerRepo> _playerRepo;
        private Mock<IMonsterRepo> _monsterRepo;
        private Mock<IGameResultRepo> _gameResultRepo;
        private Mock<IItemRepo> _itemRepo;
        private Mock<IArmorRepo> _armorRepo;
        private Mock<IAddLogic> _addLogic;
        private Mock<IWeaponRepo> _weaponRepo;
        private int _playerId;


        [SetUp]
        public void MockObjects()
        {
            _playerRepo = new Mock<IPlayerRepo>();
            _monsterRepo = new Mock<IMonsterRepo>();
            _gameResultRepo = new Mock<IGameResultRepo>();
            _itemRepo = new Mock<IItemRepo>();
            _armorRepo = new Mock<IArmorRepo>();
            _addLogic = new Mock<IAddLogic>();
            _weaponRepo = new Mock<IWeaponRepo>();
            _playerId = 1;
            _playerRepo.Setup(a => a.GetById(_playerId)).Returns(new Player()
            {
                Id = _playerId,
                Name = "Name",
                Level = 1,
                PointsEarned = 0,
                PointsForNextLevels = 50,
                MonstersBeaten = 0,
                MaxHp = 100,
                ArmorId = null,
                WeaponId = null,
                Hp = 0
            });
        }

        [Test]
        public void ReturnsTrueWhenPlayerHpEqualsZero()
        {

            var actionsService = new ActionsService(_playerRepo.Object, _monsterRepo.Object,
                _gameResultRepo.Object, _itemRepo.Object, _addLogic.Object,
                _armorRepo.Object, _weaponRepo.Object);

            var isPlayerDead = actionsService.IsPlayerDead(_playerId);
            Assert.IsTrue(isPlayerDead);
        }

        [Test]
        public void ReturnsRandomArmorId()
        {
            var randomArmorId = 10;
            _armorRepo.Setup(m => m.GetAllArmors()).Returns(new List<Armor>() { new Armor() { Id = randomArmorId } });

            var actionsService = new ActionsService(_playerRepo.Object, _monsterRepo.Object,
                _gameResultRepo.Object, _itemRepo.Object, _addLogic.Object,
                _armorRepo.Object, _weaponRepo.Object);

            var returnedRandomArmorId = actionsService.RandomArmorFound(_playerId);
            Assert.AreEqual(returnedRandomArmorId, randomArmorId);
        }

        [Test]
        public void ReturnsRandomWeaponId()
        {
            var randomWeaponId = 10;
            _weaponRepo.Setup(m => m.GetAllWeapons()).Returns(new List<Weapon>() { new Weapon() { Id = randomWeaponId } });

            var actionsService = new ActionsService(_playerRepo.Object, _monsterRepo.Object,
                _gameResultRepo.Object, _itemRepo.Object, _addLogic.Object,
                _armorRepo.Object, _weaponRepo.Object);

            var returnedRandomWeaponId = actionsService.RandomWeaponFound(_playerId);
            Assert.AreEqual(returnedRandomWeaponId, randomWeaponId);
        }

        [Test]
        public void WeaponLostTest()
        {
            var playerId = 2;
            _playerRepo.Setup(a => a.GetById(playerId)).Returns(new Player()
            {               
                WeaponId = 5
            });

            var player = _playerRepo.Object.GetById(playerId);
            var actionsService = new ActionsService(_playerRepo.Object, _monsterRepo.Object,
                _gameResultRepo.Object, _itemRepo.Object, _addLogic.Object,
                _armorRepo.Object, _weaponRepo.Object);

            actionsService.WeaponLost(playerId);
            _playerRepo.Verify(pr => pr.Update(player), Times.Once);
        }

        [Test]
        public void ArmorLostTest()
        {
            var playerId = 3;
            _playerRepo.Setup(a => a.GetById(playerId)).Returns(new Player()
            {
                ArmorId = 5
            });

            var player = _playerRepo.Object.GetById(playerId);
            var actionsService = new ActionsService(_playerRepo.Object, _monsterRepo.Object,
                _gameResultRepo.Object, _itemRepo.Object, _addLogic.Object,
                _armorRepo.Object, _weaponRepo.Object);

            actionsService.ArmorLost(playerId);
            _playerRepo.Verify(pr => pr.Update(player), Times.Once);
        }

        [Test]
        public void MagicItemLostTest()
        {
            var playerId = 4;
            _playerRepo.Setup(a => a.GetById(playerId)).Returns(new Player()
            {
                Items = new List<Item>() {new Item() }
            });

            var player = _playerRepo.Object.GetById(playerId);
            var actionsService = new ActionsService(_playerRepo.Object, _monsterRepo.Object,
                _gameResultRepo.Object, _itemRepo.Object, _addLogic.Object,
                _armorRepo.Object, _weaponRepo.Object);

            actionsService.MagicItemLost(playerId);
            _playerRepo.Verify(pr => pr.Update(player), Times.Once);
        }
    }
}