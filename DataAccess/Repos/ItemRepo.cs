﻿using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using DataAccess.Entity;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class ItemRepo : IItemRepo
    {
        private RpgDbContext _dbContext;

        public ItemRepo(RpgDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IList<Item> GetAllItems()
        {
            return _dbContext.Items.Include(p => p.Players).ToList();        
        }

        public int ItemCount()
        {
            return _dbContext.Items.Count();
        }
    }
}