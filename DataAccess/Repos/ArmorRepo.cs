﻿using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using DataAccess.Entity;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class ArmorRepo : IArmorRepo
    {
        private RpgDbContext _dbContext;

        public ArmorRepo(RpgDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IList<Armor> GetAllArmors()
        {
            return _dbContext.Armors.Include(p => p.Players).ToList();        
        }

        public int ArmorCount()
        {
            return _dbContext.Armors.Count();
        }
    }
}