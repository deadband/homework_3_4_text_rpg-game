﻿using System.Collections.Generic;
using DataAccess.Entity;

namespace DataAccess.Repos.Interfaces
{
    public interface IGameResultRepo
    {
        void AddGameResult(GameResult gameResult);
        void Delete(GameResult gameResult);
        int GameResultCount();
        IList<GameResult> GetAllGameResults();
    }
}