﻿using System.Collections.Generic;
using DataAccess.Entity;

namespace DataAccess.Repos.Interfaces
{
    public interface IMonsterRepo
    {
        void AddMonster(Monster monster);
        Monster GetById(int id);
        IList<Monster> GetAllMonsters();
        int MonsterCount();
    }
}