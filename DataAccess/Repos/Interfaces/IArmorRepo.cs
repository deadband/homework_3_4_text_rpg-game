﻿using System.Collections.Generic;
using DataAccess.Entity;

namespace DataAccess.Repos.Interfaces
{
    public interface IArmorRepo
    {
        int ArmorCount();
        IList<Armor> GetAllArmors();
    }
}