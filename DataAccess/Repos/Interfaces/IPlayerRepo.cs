﻿using System.Collections.Generic;
using DataAccess.Entity;

namespace DataAccess.Repos.Interfaces
{
    public interface IPlayerRepo
    {
        void AddPlayer(Player player);
        IList<Player> GetAllPlayers();
        Player GetById(int id);
        int PlayerCount();
        void Update(Player player);
    }
}