﻿using System.Collections.Generic;
using DataAccess.Entity;

namespace DataAccess.Repos.Interfaces
{
    public interface IWeaponRepo
    {
        IList<Weapon> GetAllWeapons();
        int WeaponCount();
    }
}