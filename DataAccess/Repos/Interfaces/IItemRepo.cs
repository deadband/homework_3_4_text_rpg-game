﻿using System.Collections.Generic;
using DataAccess.Entity;

namespace DataAccess.Repos.Interfaces
{
    public interface IItemRepo
    {
        IList<Item> GetAllItems();
        int ItemCount();
    }
}