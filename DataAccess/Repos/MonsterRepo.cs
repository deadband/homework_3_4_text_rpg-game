﻿using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using DataAccess.Entity;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class MonsterRepo : IMonsterRepo
    {
        private RpgDbContext _dbContext;

        public MonsterRepo(RpgDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddMonster(Monster monster)
        {
            _dbContext.Monsters.Add(monster);
            _dbContext.SaveChanges();
        }

        public Monster GetById(int id)
        {
            return _dbContext.Monsters.Find(id);
        }

        public IList<Monster> GetAllMonsters()
        {
            return _dbContext.Monsters.ToList();
        }

        public int MonsterCount()
        {
            return _dbContext.Monsters.Count();
        }
    }
}