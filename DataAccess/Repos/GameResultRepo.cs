﻿using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using DataAccess.Entity;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class GameResultRepo : IGameResultRepo
    {
        private RpgDbContext _dbContext;

        public GameResultRepo(RpgDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public void AddGameResult(GameResult gameResult)
        {
            _dbContext.GameResults.Add(gameResult);
            _dbContext.SaveChanges();
        }

        public IList<GameResult> GetAllGameResults()
        {
            return _dbContext.GameResults.Include(p => p.Player).ToList();
        }

        public void Delete(GameResult gameResult)
        {
            _dbContext.GameResults.Remove(gameResult);
            _dbContext.SaveChanges();
        }

        public int GameResultCount()
        {
            return _dbContext.GameResults.Count();
        }
    }
}