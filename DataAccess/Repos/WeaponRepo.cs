﻿using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using DataAccess.Entity;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class WeaponRepo : IWeaponRepo
    {
        private RpgDbContext _dbContext;

        public WeaponRepo(RpgDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IList<Weapon> GetAllWeapons()
        {
            return _dbContext.Weapons.Include(p => p.Players).ToList();        
        }

        public int WeaponCount()
        {
            return _dbContext.Weapons.Count();
        }
    }
}