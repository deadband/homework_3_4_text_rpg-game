﻿using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using DataAccess.Entity;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class PlayerRepo : IPlayerRepo
    {
        private RpgDbContext _dbContext;

        public PlayerRepo(RpgDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddPlayer(Player player)
        {
            _dbContext.Players.Add(player);
            _dbContext.SaveChanges();
        }

        public IList<Player> GetAllPlayers()
        {
            return _dbContext.Players.Include(w => w.Weapon).Include(a => a.Armor).Include(i => i.Items).ToList();        
        }

        public Player GetById(int id)
        {
            return _dbContext.Players.Find(id);
        }

        public int PlayerCount()
        {
            return _dbContext.Players.Count();
        }

        public void Update(Player player)
        {
            var dbPlayer = _dbContext.Players.Find(player.Id);
            dbPlayer = player;
            _dbContext.SaveChanges();
        }
    }
}