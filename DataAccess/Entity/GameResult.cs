﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entity
{
    public class GameResult
    {
        public int Id { get; set; }       
        public DateTime Date { get; set; }
        public int Points { get; set; }
        public int MonstersBeaten { get; set; }
        public int LevelReached { get; set; }
        public string FilePath { get; set; }
        public int PlayerId { get; set; }
        public virtual Player Player { get; set; }
    }
}