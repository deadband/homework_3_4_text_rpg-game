﻿using System.Collections.Generic;

namespace DataAccess.Entity
{
    public class Armor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Defense { get; set; }
        public virtual IList<Player> Players { get; set; }
    }
}