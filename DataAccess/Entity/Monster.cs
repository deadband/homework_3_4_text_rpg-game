﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entity
{
    public class Monster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Hp { get; set; }
        public int MaxHp { get; set; }
        public int Level { get; set; }
    }
}