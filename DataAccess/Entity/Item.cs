﻿using System.Collections.Generic;

namespace DataAccess.Entity
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ItemType ItemType { get; set; }
        public int Power { get; set; }
        public virtual IList<Player> Players { get; set; }
    }
}
