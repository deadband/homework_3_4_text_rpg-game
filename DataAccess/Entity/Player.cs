﻿using System.Collections.Generic;

namespace DataAccess.Entity
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Hp { get; set; }
        public int MaxHp { get; set; }
        public int Level { get; set; } //zamiast mocy
        public int PointsForNextLevels { get; set; }
        public int MonstersBeaten { get; set; }
        public int PointsEarned { get; set; }
        public virtual IList<GameResult> GameResults { get; set; }
        public int? WeaponId { get; set; }
        public virtual Weapon Weapon { get; set; }
        public int? ArmorId { get; set; }
        public virtual Armor Armor { get; set; }
        public virtual IList<Item> Items { get; set; }
    }
}