﻿using DataAccess.Entity;
using System.Data.Entity;

namespace DataAccess
{
    public class RpgDbContext : DbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<Monster> Monsters { get; set; }
        public DbSet<GameResult> GameResults { get; set; }
        public DbSet<Weapon> Weapons { get; set; }
        public DbSet<Armor> Armors { get; set; }
        public DbSet<Item> Items { get; set; }

        public RpgDbContext() : base("RpgDbConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<RpgDbContext, Migrations.Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //PLAYER
            modelBuilder.Entity<Player>().Property(k => k.Name)
                                         .HasColumnType("varchar");
            modelBuilder.Entity<Player>().HasIndex(k => k.Name).IsUnique();

            modelBuilder.Entity<Player>().HasMany(k => k.GameResults)
                                         .WithRequired(k => k.Player)
                                         .HasForeignKey(k => k.PlayerId);
                                         
            
            //MONSTER
            modelBuilder.Entity<Monster>().Property(k => k.Name)
                                          .HasColumnType("varchar");
            modelBuilder.Entity<Monster>().HasIndex(k => k.Name).IsUnique();

            //ITEM
            modelBuilder.Entity<Item>().HasMany(k => k.Players)
                                       .WithMany(k => k.Items)
                                       .Map(k => k.ToTable("PlayerItems"));
                                        
            //ARMOR
            modelBuilder.Entity<Armor>().HasMany(k => k.Players)
                                        .WithOptional(k => k.Armor)
                                        .HasForeignKey(k => k.ArmorId)
                                        .WillCascadeOnDelete(false);
            //WEAPON
            modelBuilder.Entity<Weapon>().Property(k => k.Name)
                                         .HasColumnType("varchar");
            modelBuilder.Entity<Weapon>().HasIndex(k => k.Name).IsUnique();

            modelBuilder.Entity<Weapon>().HasMany(k => k.Players)
                                         .WithOptional(k => k.Weapon)
                                         .HasForeignKey(k => k.WeaponId)
                                         .WillCascadeOnDelete(false);
            //GAME RESULT
            modelBuilder.Entity<GameResult>().Property(k => k.Date)
                                             .HasColumnType("datetime2");
        }
    }
} 