namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class populateDatabase : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Items VALUES('Attack amulet', 'gives + 1 to damage', 0, 2)");
            Sql("INSERT INTO Items VALUES('Attack amulet', 'gives + 2 to damage', 0, 2)");
            Sql("INSERT INTO Items VALUES('Attack amulet', 'gives + 3 to damage', 0, 3)");
            Sql("INSERT INTO Items VALUES('Attack amulet', 'gives + 4 to damage', 0, 4)");
            Sql("INSERT INTO Items VALUES('Attack amulet', 'gives + 5 to damage', 0, 5)");
            Sql("INSERT INTO Items VALUES('Cursed attack amulet', 'gives -1 to damage', 0, -1)");
            Sql("INSERT INTO Items VALUES('Cursed attack amulet', 'gives -2 to damage', 0, -2)");
            Sql("INSERT INTO Items VALUES('Cursed attack amulet', 'gives -3 to damage', 0, -3)");
            Sql("INSERT INTO Items VALUES('Cursed attack amulet', 'gives -4 to damage', 0, -4)");
            Sql("INSERT INTO Items VALUES('Cursed attack amulet', 'gives -5 to damage', 0, -5)");
            Sql("INSERT INTO Items VALUES('Defence amulet', 'gives + 1 to defence', 1, 1)");
            Sql("INSERT INTO Items VALUES('Defence amulet', 'gives + 2 to defence', 1, 2)");
            Sql("INSERT INTO Items VALUES('Defence amulet', 'gives + 3 to defence', 1, 3)");
            Sql("INSERT INTO Items VALUES('Defence amulet', 'gives + 4 to defence', 1, 4)");
            Sql("INSERT INTO Items VALUES('Defence amulet', 'gives + 5 to defence', 1, 5)");
            Sql("INSERT INTO Items VALUES('Weakness amulet', 'gives -1 to defence', 1, -1)");
            Sql("INSERT INTO Items VALUES('Weakness amulet', 'gives -2 to defence', 1, -2)");
            Sql("INSERT INTO Items VALUES('Weakness amulet', 'gives -3 to defence', 1, -3)");
            Sql("INSERT INTO Items VALUES('Weakness amulet', 'gives -4 to defence', 1, -4)");
            Sql("INSERT INTO Items VALUES('Weakness amulet', 'gives -5 to defence', 1, -5)");
            Sql("INSERT INTO Items VALUES('Good Health amulet', 'gives + 4 to max Hp', 2, 4)");
            Sql("INSERT INTO Items VALUES('Good Health amulet', 'gives + 8 to max Hp', 2, 8)");
            Sql("INSERT INTO Items VALUES('Good Health amulet', 'gives + 12 to max Hp', 2, 12)");
            Sql("INSERT INTO Items VALUES('Good Health amulet', 'gives + 16 to max Hp', 2, 16)");
            Sql("INSERT INTO Items VALUES('Good Health amulet', 'gives + 20 to max Hp', 2, 20)");
            Sql("INSERT INTO Items VALUES('Bad Health amulet', 'gives -4 to max Hp', 2, -4)");
            Sql("INSERT INTO Items VALUES('Bad Health amulet', 'gives -8 to max Hp', 2, -8)");
            Sql("INSERT INTO Items VALUES('Bad Health amulet', 'gives -12 to max Hp', 2, -12)");
            Sql("INSERT INTO Items VALUES('Bad Health amulet', 'gives -16 to max Hp', 2, -16)");
            Sql("INSERT INTO Items VALUES('Bad Health amulet', 'gives -20 to max Hp', 2, -20)");

            Sql("INSERT INTO Monsters VALUES('Monster_1A', 15, 15, 1)");
            Sql("INSERT INTO Monsters VALUES('Monster_1B', 15, 15, 1)");
            Sql("INSERT INTO Monsters VALUES('Monster_1C', 15, 15, 1)");
            Sql("INSERT INTO Monsters VALUES('Monster_2A', 30, 30, 2)");
            Sql("INSERT INTO Monsters VALUES('Monster_2B', 30, 30, 2)");
            Sql("INSERT INTO Monsters VALUES('Monster_2C', 30, 30, 2)");
            Sql("INSERT INTO Monsters VALUES('Monster_3A', 45, 45, 3)");
            Sql("INSERT INTO Monsters VALUES('Monster_3B', 45, 45, 3)");
            Sql("INSERT INTO Monsters VALUES('Monster_3C', 45, 45, 3)");
            Sql("INSERT INTO Monsters VALUES('Monster_4A', 60, 60, 4)");
            Sql("INSERT INTO Monsters VALUES('Monster_4B', 60, 60, 4)");
            Sql("INSERT INTO Monsters VALUES('Monster_4C', 60, 60, 4)");
            Sql("INSERT INTO Monsters VALUES('Monster_10', 150, 150, 10)");

            Sql("INSERT INTO Armors VALUES('Armor_1', 1)");
            Sql("INSERT INTO Armors VALUES('Armor_2', 2)");
            Sql("INSERT INTO Armors VALUES('Armor_3', 3)");
            Sql("INSERT INTO Armors VALUES('Armor_4', 4)");
            Sql("INSERT INTO Armors VALUES('Armor_5',5)");
            Sql("INSERT INTO Armors VALUES('Armor_6', 6)");
            Sql("INSERT INTO Armors VALUES('Armor_7', 7)");
            Sql("INSERT INTO Armors VALUES('Armor_8', 8)");
            Sql("INSERT INTO Armors VALUES('Armor_9', 9)");
            Sql("INSERT INTO Armors VALUES('Armor_10', 10)");

            Sql("INSERT INTO Weapons VALUES('Weapon_1', 1)");
            Sql("INSERT INTO Weapons VALUES('Weapon_2', 2)");
            Sql("INSERT INTO Weapons VALUES('Weapon_3', 3)");
            Sql("INSERT INTO Weapons VALUES('Weapon_4', 4)");
            Sql("INSERT INTO Weapons VALUES('Weapon_5' ,5)");
            Sql("INSERT INTO Weapons VALUES('Weapon_6', 6)");
            Sql("INSERT INTO Weapons VALUES('Weapon_7', 7)");
            Sql("INSERT INTO Weapons VALUES('Weapon_8', 8)");
            Sql("INSERT INTO Weapons VALUES('Weapon_9', 9)");
            Sql("INSERT INTO Weapons VALUES('Weapon_10', 10)");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Players");
            Sql("DELETE FROM Monsters");
            Sql("DELETE FROM Items");
            Sql("DELETE FROM Armors");
            Sql("DELETE FROM Weapons");
        }
    }
}
