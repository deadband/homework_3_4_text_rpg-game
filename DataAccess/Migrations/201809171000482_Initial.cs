namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GameResults",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PlayerName = c.String(),
                        Points = c.Int(nullable: false),
                        MonstersBeat = c.Int(nullable: false),
                        LevelReached = c.Int(nullable: false),
                        FilePath = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Monsters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 8000, unicode: false),
                        Hp = c.Int(nullable: false),
                        MaxHp = c.Int(nullable: false),
                        Level = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 8000, unicode: false),
                        Hp = c.Int(nullable: false),
                        MaxHp = c.Int(nullable: false),
                        Level = c.Int(nullable: false),
                        PointsForNextLevels = c.Int(nullable: false),
                        MonstersBeaten = c.Int(nullable: false),
                        PointsEarned = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Players", new[] { "Name" });
            DropIndex("dbo.Monsters", new[] { "Name" });
            DropTable("dbo.Players");
            DropTable("dbo.Monsters");
            DropTable("dbo.GameResults");
        }
    }
}
