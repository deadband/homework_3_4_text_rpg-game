namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add3ent : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Armors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Defense = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        ItemType = c.Int(nullable: false),
                        Power = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Weapons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 8000, unicode: false),
                        Damage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.PlayerItems",
                c => new
                    {
                        Item_Id = c.Int(nullable: false),
                        Player_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Item_Id, t.Player_Id })
                .ForeignKey("dbo.Items", t => t.Item_Id, cascadeDelete: true)
                .ForeignKey("dbo.Players", t => t.Player_Id, cascadeDelete: true)
                .Index(t => t.Item_Id)
                .Index(t => t.Player_Id);
            
            AddColumn("dbo.GameResults", "MonstersBeaten", c => c.Int(nullable: false));
            AddColumn("dbo.GameResults", "PlayerId", c => c.Int(nullable: false));
            AddColumn("dbo.Players", "WeaponId", c => c.Int());
            AddColumn("dbo.Players", "ArmorId", c => c.Int());
            CreateIndex("dbo.Players", "WeaponId");
            CreateIndex("dbo.Players", "ArmorId");
            CreateIndex("dbo.GameResults", "PlayerId");
            AddForeignKey("dbo.GameResults", "PlayerId", "dbo.Players", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Players", "WeaponId", "dbo.Weapons", "Id");
            AddForeignKey("dbo.Players", "ArmorId", "dbo.Armors", "Id");
            DropColumn("dbo.GameResults", "PlayerName");
            DropColumn("dbo.GameResults", "MonstersBeat");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GameResults", "MonstersBeat", c => c.Int(nullable: false));
            AddColumn("dbo.GameResults", "PlayerName", c => c.String());
            DropForeignKey("dbo.Players", "ArmorId", "dbo.Armors");
            DropForeignKey("dbo.Players", "WeaponId", "dbo.Weapons");
            DropForeignKey("dbo.PlayerItems", "Player_Id", "dbo.Players");
            DropForeignKey("dbo.PlayerItems", "Item_Id", "dbo.Items");
            DropForeignKey("dbo.GameResults", "PlayerId", "dbo.Players");
            DropIndex("dbo.PlayerItems", new[] { "Player_Id" });
            DropIndex("dbo.PlayerItems", new[] { "Item_Id" });
            DropIndex("dbo.Weapons", new[] { "Name" });
            DropIndex("dbo.GameResults", new[] { "PlayerId" });
            DropIndex("dbo.Players", new[] { "ArmorId" });
            DropIndex("dbo.Players", new[] { "WeaponId" });
            DropColumn("dbo.Players", "ArmorId");
            DropColumn("dbo.Players", "WeaponId");
            DropColumn("dbo.GameResults", "PlayerId");
            DropColumn("dbo.GameResults", "MonstersBeaten");
            DropTable("dbo.PlayerItems");
            DropTable("dbo.Weapons");
            DropTable("dbo.Items");
            DropTable("dbo.Armors");
        }
    }
}
