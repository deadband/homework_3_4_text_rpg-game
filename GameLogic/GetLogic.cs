﻿using System.IO;
using System.Linq;
using DataAccess.Repos.Interfaces;
using GameLogic.Interfaces;

namespace GameLogic
{
    public class GetLogic : IGetLogic
    { 
        private readonly IPlayerRepo _playerRepo;
        private readonly IMonsterRepo _monsterRepo;
        private readonly IGameResultRepo _gameResultRepo;

        public GetLogic(IPlayerRepo playerRepo, IMonsterRepo monsterRepo, IGameResultRepo gameResultRepo)
        {
            _playerRepo = playerRepo;
            _monsterRepo = monsterRepo;
            _gameResultRepo = gameResultRepo;
        }

        public string DisplayGameStory(int i)
        {
            var filepath = _gameResultRepo.GetAllGameResults()[i-1].FilePath;
            if (File.Exists(filepath))
            {
                return File.ReadAllText(filepath);
            }
            return "File not found";
        }

        public string DisplaySavedGameStories()
        {
            var list = "    DATA - PLAYER - POINTS\n";
            var stories = _gameResultRepo.GetAllGameResults();
            for (int i = 0; i < stories.Count(); i++)
            {
                list = list + $"{i + 1}.  {stories[i].Date} - {stories[i].Player.Name} - {stories[i].Points}\n";
            }
            return list;
        }

        public string DisplayHighscores()
        {
            var highScores = _gameResultRepo.GetAllGameResults().OrderByDescending(r => r.Points).Take(10).ToList();           
            var list = "    PLAYER - POINTS\n";
            for (int i = 0; i < highScores.Count(); i++)
            {
                list = list + $"{i + 1}.  {highScores[i].Player.Name} - {highScores[i].Points}\n";
            }
            return list;
        }

        public string DisplayPlayers()
        {
            var message = string.Empty;
            var players = _playerRepo.GetAllPlayers();
            for (int i = 0; i < players.Count; i++)
            {
                message = message + $"{i+1}. {players[i].Name}\n";
            }
            return message;
        }

        public void UpdateGameResultsInDataBase() // jeżeli ktoś ręcznie usunął pliki usuwa gameResults w bazie lub jezeli 
        {                                         // plik loga stworzył się ale nie zapisał sie wynik gry w game results to usuwa plik  
            var gameResults = _gameResultRepo.GetAllGameResults();
            var gameResultsFilesPaths = gameResults.Select(g => g.FilePath);
            var existingFilesPaths = Directory.GetFiles(@"..\..\..\logs\");
            foreach (var dbFilePath in gameResultsFilesPaths)
            {
                if (!existingFilesPaths.Contains(dbFilePath))
                {
                    var gameResultToDelete = gameResults.SingleOrDefault(r => r.FilePath == dbFilePath);
                    _gameResultRepo.Delete(gameResultToDelete);
                }
            }
            foreach (var existingFilePath in existingFilesPaths)
            {
                if (!gameResultsFilesPaths.Contains(existingFilePath))
                {
                    File.Delete(existingFilePath);
                }
            }
        }

        public int GetPlayerCount()
        {
            return _playerRepo.PlayerCount();
        }

        public int GetMonsterCount()
        {
            return _monsterRepo.MonsterCount();
        }

        public int GetGameResultCount()
        {
            return _gameResultRepo.GameResultCount();
        }     
    }
}