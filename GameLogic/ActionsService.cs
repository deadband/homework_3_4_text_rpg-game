﻿using System;
using System.Linq;
using System.Collections.Generic;
using DataAccess;
using DataAccess.Entity;
using DataAccess.Repos.Interfaces;
using GameLogic.Interfaces;

namespace GameLogic
{
    public class ActionsService : IActionsService
    {
        private readonly IPlayerRepo _playerRepo;
        private readonly IMonsterRepo _monsterRepo;
        private readonly IGameResultRepo _gameResultRepo;
        private readonly IItemRepo _itemRepo;
        private readonly IArmorRepo _armorRepo;
        private readonly IAddLogic _addLogic;
        private readonly IWeaponRepo _weaponRepo;
        private IList<ILogStoryLine> _logStoryLine;
        private Random _random;

        public ActionsService(IPlayerRepo playerRepo, IMonsterRepo monsterRepo, IGameResultRepo gameResultRepo,
                              IItemRepo itemRepo, IAddLogic addLogic, IArmorRepo armorRepo, IWeaponRepo weaponRepo)
        {
            _random = new Random();
            _playerRepo = playerRepo;
            _monsterRepo = monsterRepo;
            _gameResultRepo = gameResultRepo;
            _itemRepo = itemRepo;
            _addLogic = addLogic;
            _armorRepo = armorRepo;
            _weaponRepo = weaponRepo;
            _logStoryLine = new List<ILogStoryLine>();
        }

        public bool IsPlayerDead(int playerId)
        {
            return _playerRepo.GetById(playerId).Hp == 0;
        }

        public int SetActualPlayerAndStoryFile(int i)
        {
            var player = _playerRepo.GetAllPlayers()[i - 1];
            ResetPlayer(player);                // reset wsystkich parametrów bohatera z bazy do nowej gry
            _logStoryLine = new List<ILogStoryLine>
            {
                new LogStoryLineToConsole(),
                new LogStoryLineToFile(player.Name)
            };
            return player.Id;
        }

        public int SetRandomMonster(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            DisplayPlayersState(player);
            var randomMonster = _monsterRepo.GetAllMonsters()[_random.Next(0, _monsterRepo.MonsterCount())];
            var message = $"{player.Name} meets {randomMonster.Name}, Lvl: {randomMonster.Level}, HP: {randomMonster.Hp}";
            LogToConsoleAndFile(message);
            return randomMonster.Id;
        }

        public void SimulateFight(int monsterId, int playerId)
        {
            var monster = _monsterRepo.GetById(monsterId);
            var player = _playerRepo.GetById(playerId);
            DisplayPlayersState(player);
            var whoseTurn = _random.Next(0, 2);
            while (player.Hp > 0 && monster.Hp > 0)
            {
                switch (whoseTurn)
                {
                    case 0:
                        var damage = CalculateDamageDealtByPlayer(player);
                        monster.Hp = CaclulateHpAfterAction(monster.Hp, monster.MaxHp, -damage);
                        var message = $"{monster.Name} got hit. {player.Name} deals {damage} HP damage. HP left {monster.Hp}.";
                        whoseTurn = -whoseTurn + 1;
                        LogToConsoleAndFile(message);

                        break;
                    case 1:
                        damage = CalculateDamageDealtByMonster(player, monster);
                        player.Hp = CaclulateHpAfterAction(player.Hp, player.MaxHp, -damage);
                        message = $"{player.Name} got hit. {monster.Name} deals {damage} HP damage. HP left {player.Hp}.";
                        whoseTurn = -whoseTurn + 1;
                        LogToConsoleAndFile(message);
                        break;
                }
            }
            monster.Hp = monster.MaxHp;
            if (player.Hp > 0) EndFight(player, monster);
        }

        public void Retreat(int monsterId, int playerId)
        {
            var monster = _monsterRepo.GetById(monsterId);
            var player = _playerRepo.GetById(playerId);
            DisplayPlayersState(player);
            var message = string.Empty;
            var gotHit = _random.NextDouble();
            switch (gotHit)
            {
                case double n when n < 0.5:
                    message = "Successful retreat!";
                    break;
                default:
                    var hpLost = monster.Level * _random.Next(3, 8);
                    player.Hp = CaclulateHpAfterAction(player.Hp, player.MaxHp, -hpLost);
                    message = $"{player.Name} got hit while retreat and lost {hpLost} HP, HP left: {player.Hp}";
                    break;
            }
            LogToConsoleAndFile(message);
        }

        public void HpPotionFound(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            DisplayPlayersState(player);
            var hpRestored = player.Level * _random.Next(5, 11);
            player.Hp = CaclulateHpAfterAction(player.Hp, player.MaxHp, hpRestored);
            var message = $"{player.Name} found potion restoring {hpRestored} HP, HP left: {player.Hp}.";
            LogToConsoleAndFile(message);
        }
         
        public void MagicItemFound(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            DisplayPlayersState(player);
            var randomItem = _itemRepo.GetAllItems()[_random.Next(0, _itemRepo.ItemCount())];
            var message = $"{player.Name} found magic item  {randomItem.Name} that {randomItem.Description}.";
            if (randomItem.ItemType == ItemType.Hp && player.MaxHp + randomItem.Power > 0)
            {
                player.MaxHp = Math.Max(1, player.MaxHp + randomItem.Power);
                message += $"\n{player.Name} max Hp i now {player.MaxHp}.";
            }
            player.Items.Add(randomItem);
            _playerRepo.Update(player);
            LogToConsoleAndFile(message);
        }

        public void MaxHpOrLevelUpItemFound(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            DisplayPlayersState(player);
            var message = string.Empty;
            var itemType = _random.NextDouble();
            switch (itemType)
            {
                case double n when(n >= 0.35 && n < 0.5):
                    message = $"{player.Name} found magic level-up item. " + LevelUp(player);
                    break;
                default:
                    player.Hp = player.MaxHp;
                    message = $"{player.Name} found magic item restoring your max HP. Current HP: {player.Hp}";
                    break;
            }
            LogToConsoleAndFile(message);
        }

        public int RandomWeaponFound(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            DisplayPlayersState(player);
            var randomWeapon = _weaponRepo.GetAllWeapons()[_random.Next(0, _weaponRepo.WeaponCount())];
            var message = $"{player.Name} found {randomWeapon.Name}, Damage: {randomWeapon.Damage}";
            if (player.WeaponId == null)
            {
                message += $"\n{player.Name} is not carrying any weapon now. ";
            }
            else
            {
                message += $"\n{player.Name} actual weapon is {player.Weapon.Name}, Damage:  {player.Weapon.Damage} ";
            }
            LogToConsoleAndFile(message);
            return randomWeapon.Id;
        }

        public void ReplaceWeapon(int weaponId, int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            player.WeaponId = weaponId;
            _playerRepo.Update(player);
            var message = $"{player.Name} now has a new weapon {player.Weapon.Name}";
            LogToConsoleAndFile(message);
        }

        public int RandomArmorFound(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            DisplayPlayersState(player);
            var randomArmor = _armorRepo.GetAllArmors()[_random.Next(0, _armorRepo.ArmorCount())];
            var message = $"{player.Name} found {randomArmor.Name}, Defence: {randomArmor.Defense}";
            if (player.ArmorId == null)
            {
                message += $"\n{player.Name} is not wearing any armor now. ";
            }
            else
            {
                message += $"\n{player.Name} actual armor is {player.Armor.Name}, Defence:  {player.Armor.Defense} ";
            }
            LogToConsoleAndFile(message);
            return randomArmor.Id;
        }

        public void ReplaceArmor(int armorId, int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            player.ArmorId = armorId;
            _playerRepo.Update(player);
            var message = $"{player.Name} now has a new armor {player.Armor.Name}";
            LogToConsoleAndFile(message);
        }

        public void WeaponLost(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            if (player.WeaponId != null)
            {
                DisplayPlayersState(player);
                player.WeaponId = null;
                _playerRepo.Update(player);
                var message = $"Accidentally {player.Name} lost his Armor. To increase attack find another one.";
                LogToConsoleAndFile(message);
            }
        }

        public void ArmorLost(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            if (player.ArmorId != null)
            {
                DisplayPlayersState(player);
                player.ArmorId = null;
                _playerRepo.Update(player);
                var message = $"Accidentally {player.Name} lost his Armor. To increase defence find another one.";
                LogToConsoleAndFile(message);
            }
        }

        public void MagicItemLost(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            if (player.Items.Any())
            {
                DisplayPlayersState(player);
                var itemToLooseNumber = _random.Next(0, player.Items.Count);
                var itemToLoose = player.Items[itemToLooseNumber];
                var message = $"Accidentally {player.Name} lost {itemToLoose.Name} that {itemToLoose.Description}.";
                if (itemToLoose.ItemType == ItemType.Hp)
                {
                    player.MaxHp = Math.Max(1, player.MaxHp - itemToLoose.Power);
                    player.Hp = Math.Min(player.Hp, player.MaxHp);
                    message += $"\n{player.Name} max HP is now {player.MaxHp}";
                }
                player.Items.Remove(itemToLoose);
                _playerRepo.Update(player);
                LogToConsoleAndFile(message);
            }
        }

        public void RandomNegativeEvent(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            DisplayPlayersState(player);
            var message = string.Empty;
            var possibleEvent = _random.Next(1, player.Level + 1);
            var hpLost = possibleEvent * player.Level * _random.Next(1, 3);
            player.Hp = CaclulateHpAfterAction(player.Hp, player.MaxHp, -hpLost);

            switch (possibleEvent)
            {
                case 1:
                    message = $"{player.Name} got hit by tree branch and lost {hpLost} HP. HP left: {player.Hp}";
                    break;
                case 2:
                    message = $"{player.Name} got hit by a wild duck and lost {hpLost} HP. HP left: {player.Hp}";
                    break;
                case 3:
                    message = $"{player.Name} got bit by a spider and lost {hpLost} HP. HP left: {player.Hp}";
                    break;
                case 4:
                    message = $"{player.Name} got bit by a snake and lost {hpLost} HP. HP left: {player.Hp}";
                    break;
                case 5:
                    message = $"{player.Name} got bit by a wild tiger and lost {hpLost} HP. HP left: {player.Hp}";
                    break;
                default:
                    message = $"{player.Name} got hit by a dark rider and lost {hpLost} HP. HP left: {player.Hp}";
                    break;
            }
            LogToConsoleAndFile(message);
        }

        public void GameOver(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            _addLogic.CreateGameResult(player);
            var message = $"{player.Name} is dead. Game Over. \n" +
                    $"{player.Name} earned {player.PointsEarned} points, \n" +
                    $"beat {player.MonstersBeaten} monsters, reached {player.Level} level."; 
            LogToConsoleAndFile(message);
        }

        public void NothingFoundAfterFight(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            var message = $"{player.Name} finds nothing interesting after fight.";
            LogToConsoleAndFile(message);
        }

        public void ItemDroppedByPlayer(int playerId)
        {
            var player = _playerRepo.GetById(playerId);
            var message = $"{player.Name} dropped found item";
            LogToConsoleAndFile(message);
        }

        private void DisplayPlayersState(Player player)
        {
            Console.Clear();
            Console.WriteLine($"{player.Name}, Lvl: {player.Level} {player.Hp}/{player.MaxHp} HP, {player.PointsEarned}/{player.PointsForNextLevels} points, {player.MonstersBeaten} monsters beaten\n");
        }

        private int CalculateDamageDealtByMonster(Player player, Monster monster)
        {
            var additionalItemDefence = 0;
            var additionalArmorDefence = 0;
            if (player.Items.Where(i => i.ItemType == ItemType.Defence).Any())
                additionalItemDefence = player.Items.Where(i => i.ItemType == ItemType.Defence).Select(i => i.Power).Sum();
            if (player.Armor != null)
                additionalArmorDefence = player.Armor.Defense;
            return Math.Max(1, monster.Level * _random.Next(3, 6) - additionalItemDefence - additionalArmorDefence);
        }

        private int CalculateDamageDealtByPlayer(Player player)
        {
            var additionalItemDamage = 0;
            var additionalWeaponDamage = 0;
            if (player.Items.Where(i => i.ItemType == ItemType.Attack).Any())
                additionalItemDamage = player.Items.Where(i => i.ItemType == ItemType.Attack).Select(i => i.Power).Sum();
            if (player.Weapon != null)
                additionalWeaponDamage = player.Weapon.Damage;
            return player.Level * _random.Next(4, 7) + additionalItemDamage + additionalWeaponDamage;
        }

        private int CaclulateHpAfterAction(int hp, int hpMax,int hpChange)
        {
            hp += hpChange;
            return Math.Min(hpMax, Math.Max(0, hp)); //zapobiega Hp <0 i Hp > MaxHp
        }

        private void EndFight(Player player, Monster monster)
        {
            DisplayPlayersState(player);
            player.MonstersBeaten++;
            player.PointsEarned += monster.Level * 5;
            var message = $"{player.Name} killed {monster.Name}, {monster.Level * 5} points earned, total points : {player.PointsEarned}";
            if (player.PointsEarned >= player.PointsForNextLevels)
            {
                message = message + LevelUp(player);
            }
            LogToConsoleAndFile(message);
        }

        private string LevelUp(Player player)
        {
            player.Level++;
            player.MaxHp = 100 + 20 * (player.Level - 1);
            if (player.PointsForNextLevels > player.PointsEarned)
            {
                player.PointsEarned = player.PointsForNextLevels;
            }
            player.PointsForNextLevels += player.Level * 50;
            return $"\n{player.Name} got promoted to {player.Level} level. Next level promotion after earning {player.PointsForNextLevels} points\n" +
                          $"{player.Name} increases max HP to {player.MaxHp} points.";
        }

        private void LogToConsoleAndFile(string message)
        {
            foreach (var item in _logStoryLine)
                item.LogStoryLine(message);
        }

        private void ResetPlayer(Player player)
        {
            player.Hp = 100;
            player.MaxHp = 100;
            player.Level = 1;
            player.MonstersBeaten = 0;
            player.PointsEarned = 0;
            player.PointsForNextLevels = 50;
            player.ArmorId = null;
            player.WeaponId = null;
            player.Items = new List<Item>();
            _playerRepo.Update(player);
        }
    }
}