﻿   using System;
using System.IO;

namespace GameLogic
{
    class LogStoryLineToFile : ILogStoryLine
    {
        public static string _filePath;

        public LogStoryLineToFile(string playersName)
        {
            var directory = @"..\..\..\logs\";
            var fileName = (playersName + "_" + DateTime.Now).Replace(":", "/")
                                                             .Replace("/", "")
                                                             .Replace(" ", "") + ".txt";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            _filePath = Path.Combine(directory, fileName);
        }

        public void LogStoryLine(string message)
        {
            using (var logToFile = File.AppendText(_filePath))
            {
                logToFile.WriteLine(message);
            }
        }
    }
}