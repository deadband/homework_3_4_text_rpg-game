﻿namespace GameLogic.Interfaces
{
    public interface IActionsService
    {
        void ArmorLost(int playerId);
        void GameOver(int playerId);
        void HpPotionFound(int playerId);
        bool IsPlayerDead(int playerId);
        void ItemDroppedByPlayer(int playerId);
        void MagicItemFound(int playerId);
        void MagicItemLost(int playerId);
        void MaxHpOrLevelUpItemFound(int playerId);
        void NothingFoundAfterFight(int playerId);
        int RandomArmorFound(int playerId);
        void RandomNegativeEvent(int playerId);
        int RandomWeaponFound(int playerId);
        void ReplaceArmor(int armorId, int playerId);
        void ReplaceWeapon(int weaponId, int playerId);
        void Retreat(int monsterId, int playerId);
        int SetActualPlayerAndStoryFile(int i);
        int SetRandomMonster(int playerId);
        void SimulateFight(int monsterId, int playerId);
        void WeaponLost(int playerId);
    }
}