﻿namespace GameLogic
{
    interface ILogStoryLine
    {
        void LogStoryLine(string message);
    }
}