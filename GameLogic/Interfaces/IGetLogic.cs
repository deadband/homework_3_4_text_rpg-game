﻿namespace GameLogic.Interfaces
{
    public interface IGetLogic
    {
        string DisplayGameStory(int i);
        string DisplayHighscores();
        string DisplayPlayers();
        string DisplaySavedGameStories();
        int GetGameResultCount();
        int GetMonsterCount();
        int GetPlayerCount();
        void UpdateGameResultsInDataBase();
    }
}