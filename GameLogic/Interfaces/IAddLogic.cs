﻿using DataAccess.Entity;

namespace GameLogic.Interfaces
{
    public interface IAddLogic
    {
        void CreateGameResult(Player player);
        string CreateMonster(string name, int level);
        string CreatePlayer(string name);
    }
}