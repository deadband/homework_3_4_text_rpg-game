﻿using Ninject.Modules;
using DataAccess.Repos;
using DataAccess;
using GameLogic.Interfaces;
using DataAccess.Repos.Interfaces;

namespace GameLogic
{
    public class DiModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IGetLogic>().To<GetLogic>();
            Bind<IAddLogic>().To<AddLogic>();
            Bind<IActionsService>().To<ActionsService>();
            Bind<IGameResultRepo>().To<GameResultRepo>();
            Bind<IPlayerRepo>().To<PlayerRepo>();
            Bind<IMonsterRepo>().To<MonsterRepo>();
            Bind<IItemRepo>().To<ItemRepo>();
            Bind<IWeaponRepo>().To<WeaponRepo>();
            Bind<IArmorRepo>().To<ArmorRepo>();
            Bind<RpgDbContext>().To<RpgDbContext>().InSingletonScope();
        }
    }
}