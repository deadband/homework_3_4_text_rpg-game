﻿using System;
using System.Linq;
using DataAccess.Entity;
using DataAccess.Repos.Interfaces;
using GameLogic.Interfaces;

namespace GameLogic
{
    public class AddLogic : IAddLogic
    {
        private readonly IPlayerRepo _playerRepo;
        private readonly IMonsterRepo _monsterRepo;
        private readonly IGameResultRepo _gameResultRepo;

        public AddLogic(IPlayerRepo playerRepo, IMonsterRepo monsterRepo, IGameResultRepo gameResultRepo)
        {
            _playerRepo = playerRepo;
            _monsterRepo = monsterRepo;
            _gameResultRepo = gameResultRepo;
        }

        public string CreatePlayer(string name)
        {
            if (_playerRepo.GetAllPlayers().SingleOrDefault(p => p.Name == name) != null)
            {
                return "New player not added. Player with name provided already Exists.";
            }
            var player = new Player()
            {
                Name = name,
                Hp = 100,
                MaxHp = 100,
                Level = 1,
                MonstersBeaten = 0,
                PointsEarned = 0,
                PointsForNextLevels = 50
            };
            _playerRepo.AddPlayer(player);
            return $"Player {player.Name}, Hp = {player.MaxHp}, Level 1 created.";
        }

        public string CreateMonster(string name, int level)
        {
            if (_monsterRepo.GetAllMonsters().SingleOrDefault(p => p.Name == name) != null)
            {
                return "New monster not created. Monster with name provided already Exists.";
            }
            var monster = new Monster()
            {
                Name = name,
                Level = level,
                Hp = level * 15,
                MaxHp = level * 15
            };
            _monsterRepo.AddMonster(monster);
            return $"Monster {monster.Name}  created \n Hp = {monster.Hp} \n Level {monster.Level}.";
        }

        public void CreateGameResult(Player player)
        {
            var gameResult = new GameResult()
            {
                Date = DateTime.Now,
                PlayerId = player.Id,
                LevelReached = player.Level,
                MonstersBeaten = player.MonstersBeaten,
                Points = player.PointsEarned,
                FilePath = LogStoryLineToFile._filePath
            };
            _gameResultRepo.AddGameResult(gameResult);
        }
    }
}