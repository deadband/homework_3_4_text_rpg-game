﻿using System;

namespace GameLogic
{
    class LogStoryLineToConsole : ILogStoryLine
    {
        public void LogStoryLine(string message)
        {
            Console.WriteLine(message);
            if (!message.Contains("meets") && !message.Contains("actual") && !message.Contains("is not"))
            {
                Console.ReadKey();
            }
        }
    }
}